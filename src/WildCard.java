package src;

public class WildCard extends Card {
	public WildCard(ColorCard color) {
		super(color);
	}

	@Override
	public boolean cardPlay(Card lastCard) {
		return false;
	}
}
