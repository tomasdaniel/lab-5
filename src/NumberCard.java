package src;

public class NumberCard extends Card {
	private Numbers number;

    public NumberCard(ColorCard color, Numbers number) {
        super(color);
        this.number = number;
    }

    @Override
    public boolean cardPlay(Card lastCard) {
        return false;
    }
    
}
