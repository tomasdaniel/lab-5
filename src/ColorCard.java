package src;

public enum ColorCard {
    NONE,
    RED,
    GREEN,
    BLUE,
    YELLOW;
}