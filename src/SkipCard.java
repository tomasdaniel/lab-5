package src;

public class SkipCard extends Card {

	public SkipCard(ColorCard color) {
		super(color);
	}

	@Override
    public boolean cardPlay(Card lastCard) {
        return false;
    }

    
}
