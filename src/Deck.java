package src;

import java.util.ArrayList;
import java.util.Random;

public class Deck {
	private ArrayList<Card> deck = new ArrayList<>();
	
	public Deck() {
		deck.add(new WildCard(ColorCard.NONE));
		deck.add(new WildCard(ColorCard.NONE));
		deck.add(new WildCard(ColorCard.NONE));
		deck.add(new WildCard(ColorCard.NONE));
		
		deck.add(new WildPickupFourCard(ColorCard.NONE));
		deck.add(new WildPickupFourCard(ColorCard.NONE));
		deck.add(new WildPickupFourCard(ColorCard.NONE));
		deck.add(new WildPickupFourCard(ColorCard.NONE));
		
		
		for (ColorCard color : ColorCard.values()) {
			if (color == ColorCard.NONE) {
				continue;
			}
			
			for (Numbers number : Numbers.values()) {
				deck.add(new NumberCard(color, number));
				deck.add(new NumberCard(color, number));
			}
		}
		
		for (ColorCard color : ColorCard.values()) {
			if (color == ColorCard.NONE) {
				continue;
			}
			
			deck.add(new PickupTwoCard(color));
			deck.add(new PickupTwoCard(color));
			
			deck.add(new ReverseCard(color));
			deck.add(new ReverseCard(color));
			
			deck.add(new SkipCard(color));
			deck.add(new SkipCard(color));
		}
	}
	
	public void shuffle() {
		Random random = new Random();
		for (int i = 0; i < deck.size(); i++){
			int index = random.nextInt(deck.size() - 1);
			
			Card temp = deck.get(index);
			deck.set(index, deck.get(i));
			deck.set(i, temp);
		}
	}
	
	public void addToDeck(Card card) {
		deck.add(card);
	}
	
	public Card draw() {
		return deck.remove(0);
	}
	
	public int getSize() {
		return deck.size();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Card card : deck) {
			sb.append(card.toString() + "\n");
		}
		
		return sb.toString();
	}
}
