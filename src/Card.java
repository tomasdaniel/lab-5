package src;

public abstract class Card implements PlayCard {
   protected ColorCard color;

   protected Card(ColorCard color) {
      this.color = color;
   }

   public String toString() {
      return this.getClass().getSimpleName() + " " + this.color;
   }
}
