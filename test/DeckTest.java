package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import src.Deck;

public class DeckTest {
    // Test the Deck class by creating a deck of cards and comparing the deck to a known deck.
    @Test
    public void createDeck() {
        Deck deck = new Deck();
        assertEquals(112, deck.getSize());
    }

    // Creating a deck manually is not too reasonable (112 cards!)
    // So, test the deck by printing it out.
    @Test
        public void printDeck() {
        Deck deck = new Deck();
        System.out.println(deck);
    }

    // Testing the shuffle method by printing it out before and after shuffling.
    // As there is a small chance that the shuffled deck will be the same as the original
    @Test
    public void shuffleDeck() {
        Deck deck = new Deck();
        System.out.println("========= Unshuffled deck ==========");
        System.out.println(deck);
        deck.shuffle();
        System.out.println("======== Shuffled deck =============");
        System.out.println(deck);
    }
}
